window.onscroll = function () { scrollFunction() }

function scrollFunction () {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById('menu').style.height = '200px'
    document.getElementById('logo').style.height = '100px'
    document.getElementById('display').style.display = 'none'
  } else {
    document.getElementById('menu').style.height = '475px'
    document.getElementById('logo').style.height = '175px'
    document.getElementById('display').style.display = 'block'
  }
}
